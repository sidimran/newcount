import React, { Component } from "react";

//input :liked :boolean
//output : onClick

//we are changing to stateless functional component because we do not have any state, any helper method, any event handler, we only have render method -> so we are going to keep in stateless functional component and simply the code

const Like  = (props) => {

    let Classes = "fa fa-heart";
    if (!props.liked) Classes += "-o";
    return (
      <i
        onClick={props.onClick}
        style={{ cursor: "pointer" }}
        className={Classes}
        aria-hidden="true"
      ></i>
  );
}
 
export default Like;



// class Like extends Component {
//   render() {
    
//     );
//   }

