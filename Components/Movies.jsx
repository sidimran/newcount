import React, { Component } from "react";
import { getMovies } from "../service/fakeMovieService";
import Pagination from "./Pagination";
import Like from "./SolutionLikeComponent/like";

class Movies extends Component {
  state = {
    movies: getMovies(),
    pageSize: 3,
    currentPage :1,
  };

  handleDelete = (movie) => {
    console.log("Called", movie);
    const movies = this.state.movies.filter((movi) => movi._id !== movie._id);
    this.setState({ movies: movies });
  };

  handleLike = (movie) => {
    //console.log("Liked Clicked", movie);
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  handlePageChange = (page) => {
    // console.log(page);
    this.setState({currentPage : page})
  };

  render() {
    const count = this.state.movies.length;
    const {pageSize,currentPage} = this.state;  

    if (count === 0) return <p>There are no movies in Database</p>;

    return (
      <React.Fragment>
        {/* <p>Showing {this.state.movies.length} movies in the DataBase</p> */}{" "}
        {/*can be written in*/}
        <p> Showing {count} movies in the DataBase</p>
        <table className="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Genre</th>
              <th>Stock</th>
              <th>Rate</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.state.movies.map((movie) => {
              return (
                <tr key={movie._id}>
                  <td>{movie.title}</td>
                  <td>{movie.genre.name}</td>
                  <td>{movie.numberInStock}</td>
                  <td>{movie.dailyRentalRate}</td>
                  <td>
                    <Like
                      liked={movie.liked}
                      onClick={() => this.handleLike(movie)}
                    />
                  </td>
                  <td>
                    <button
                      onClick={() => this.handleDelete(movie)}
                      className="btn btn-danger btn-sm "
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          itemCount={count}
          pageSize={pageSize}
          onPageChange={this.handlePageChange}
          currentPage ={currentPage}
        />
      </React.Fragment>
    );
  }
}

export default Movies;
